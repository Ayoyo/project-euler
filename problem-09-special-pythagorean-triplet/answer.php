<?php

/**
 * A Pythagorean triplet is a set of three natural numbers, a < b < c, for which
 *              a^2 + b^2 = c^2
 * For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.
 * There exists exactly one Pythagorean triplet for which a + b + c = 1000
 * Find the product abc.
 */

for ($m = 2; $m < 50; $m++) {
    for ($n = 1; $n < $m; $n++) {
        list($a, $b, $c) = findVariablesOfPythagoreanTriple($m, $n);

        if (isMatchFormula($a, $b, $c)) {
            echo $a * $b * $c;
            break;
        }
    }
}

/**
 * @ref     http://mathforum.org/library/drmath/view/55811.html
 * @return  array
 */
function findVariablesOfPythagoreanTriple($m, $n)
{
    $squareN = pow($n, 2);
    $squareM = pow($m, 2);

    return [
        // a
        $squareM - $squareN,
        // b
        2 * $m * $n,
        // c
        $squareM + $squareN,
    ];
}

function isMatchFormula($a, $b, $c)
{
    return (
        $a + $b == 1000 - $c
    );
}