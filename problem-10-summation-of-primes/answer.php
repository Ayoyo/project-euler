<?php

/**
 * The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
 * Find the sum of all the primes below two million.
 */

$primes = generatePrimeSequence(2 * 1000 * 1000);
echo array_sum($primes);

/**
 * @ref    https://www.geeksforgeeks.org/sieve-of-eratosthenes/
 * @param  int  $n
 */
function generatePrimeSequence($limit) 
{ 
    // Create a boolean array "prime[2..n]"  
    // and initialize all entries it as true. 
    // A value in prime[i] will finally be  
    // false if i is Not a prime, else true. 
    $primes = array_fill(2, $limit - 1, true);

    for ($p = 2; $p * $p <= $limit; $p++) { 
        // If prime[p] is not changed,  
        // then it is a prime 
        if ($primes[$p] == true) { 
            // Update all multiples of p 
            for ($i = $p * $p; $i <= $limit; $i += $p) {
                $primes[$i] = false;
            }
        }
    }

    // Swap value with key if value is true/prime
    array_walk($primes, function(&$value, $key) {
        $value = $value ? $key : $value;
    });

    return array_filter($primes, function($number) {
        return is_int($number);
    });
}