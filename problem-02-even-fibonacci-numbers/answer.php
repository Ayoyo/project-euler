<?php

/**
 * Each new term in the Fibonacci sequence is generated by adding the previous two terms.
 * By starting with 1 and 2, the first 10 terms will be:
 * 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...
 * By considering the terms in the Fibonacci sequence whose values do not exceed four million,
 * find the sum of the even-valued terms.
 */

function sum($number1, $number2) {
    return $number1 + $number2;
}

function generateFibonacciSequence($exceedValue = 10) {
    $sequence = [];
    $x = 0;
    $y = 1;

    if ($exceedValue <= 2) {
        throw new Exception('Exceed value can\'t lower than 2');
    }

    $z = sum($x, $y);

    while ($z < $exceedValue) {
        $sequence[]  = $z;
        list($x, $y) = [$y, $z];
        $z           = sum($x, $y);
    }

    return $sequence;
}

function sumOfEvenNumbers($numbers) {
    if (empty($numbers)) {
        return 0;
    }

    $evenNumbers = array_filter($numbers, function($number) {
        return $number % 2 == 0;
    });

    return array_reduce($evenNumbers, function($sum, $number) {
        return $sum += $number;
    }, 0);
}

$fibonacci = generateFibonacciSequence(4 * 1000 * 1000);
$sum       = sumOfEvenNumbers($fibonacci);

var_dump($sum);